/* eslint-disable react-hooks/exhaustive-deps */
import React, {
  useCallback,
  useState,
  useRef,
  memo,
  useMemo,
  useEffect,
} from 'react';
import {
  View,
  FlatList,
  StyleSheet,
  StatusBar,
  Text,
  RefreshControl,
} from 'react-native';

import FeatherIcon from 'react-native-vector-icons/Feather';
import {useDispatch, useSelector} from 'react-redux';

import {TextInput} from 'react-native-gesture-handler';
import {SafeAreaView} from 'react-native-safe-area-context';
import useDebounce from '../hooks/useDebounce';
import MediaPerson from '../components/MediaPerson';
import MediaMovie from '../components/MediaMovie';
import MediaTV from '../components/MediaTV';

// import action
import searchAction from '../redux/actions/search';

const styles = StyleSheet.create({
  title: {
    fontSize: 24,
    fontWeight: 'bold',
    color: 'white',
    marginLeft: 16,
  },
  subtitle: {
    fontSize: 16,
    color: '#ffffff55',
    marginLeft: 16,
    marginBottom: 8,
  },
  horizontalSpacing: {
    width: 8,
  },
  horizontalCoverList: {
    paddingHorizontal: 16,
    paddingBottom: 16,
  },
  flatList: {
    marginBottom: 16,
    // flex: 1,
  },
  placeholderContainer: {
    backgroundColor: '#ffffff22',
  },
  searchBarTextInput: {
    height: 60,
    borderBottomColor: 'gray',
    borderWidth: 1,
    color: 'white',
    paddingHorizontal: 16,
  },
  emptyContainer: {
    margin: 24,
    alignItems: 'center',
    justifyContent: 'center',
    flex: 1,
  },
  emptyText: {
    color: 'gray',
  },
  contentContainer: {
    marginVertical: 16,
    paddingBottom: 60,
  },
});

const KEY_EXTRACTOR = (item) => String(item.id);

const Search = () => {
  const [query, setQuery] = useState('');
  const debouncedQuery = useDebounce(query, 1000);
  const [dataSource, setDataSource] = useState(null);

  const flatlistRef = useRef(null);
  const dispatch = useDispatch();

  const {data, isLoadingSearch, isSuccess} = useSelector(
    (state) => state.search,
  );

  React.useEffect(() => {
    if (isSuccess && !isLoadingSearch) {
      setDataSource(data);
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [isSuccess, isLoadingSearch]);

  const requestSearch = useCallback(async () => {
    if (debouncedQuery) {
      try {
        await dispatch(
          searchAction.search(
            'https://api.themoviedb.org/3/search/multi',
            {
              api_key: '8c6570a65b2a0c3dadfdac7de4cd5d50',
              query: debouncedQuery,
              language: 'en-US',
            },
            'GET_SEARCH',
          ),
        );
      } catch (ex) {
        console.warn(ex);
      }
    }
  }, [debouncedQuery]);

  const renderItem = useCallback(({item}) => {
    switch (item.media_type) {
      case 'person':
        return <MediaPerson person={item} />;
      case 'movie':
        return <MediaMovie movie={item} />;
      case 'tv':
        return <MediaTV tvShow={item} />;
      default:
        return null;
    }
  }, []);

  useEffect(() => {
    requestSearch();
  }, [debouncedQuery]);

  const renderSearchBar = () => (
    <View style={styles.placeholderContainer}>
      <TextInput
        placeholder="Type something to search"
        placeholderTextColor="gray"
        autoFocus
        style={styles.searchBarTextInput}
        onChangeText={setQuery}
        value={query}
      />
    </View>
  );

  const renderEmptySearchResults = useCallback(
    () => (
      <View style={styles.emptyContainer}>
        <FeatherIcon name="inbox" size={128} color="gray" />
        <Text style={styles.emptyText}>No results found</Text>
      </View>
    ),
    [],
  );

  return (
    <SafeAreaView>
      <StatusBar backgroundColor="#00000044" translucent />
      {renderSearchBar()}
      <FlatList
        ref={flatlistRef}
        refreshControl={
          <RefreshControl
            refreshing={isLoadingSearch}
            onRefresh={requestSearch}
          />
        }
        contentContainerStyle={styles.contentContainer}
        keyExtractor={KEY_EXTRACTOR}
        ListEmptyComponent={renderEmptySearchResults}
        data={dataSource?.results}
        renderItem={renderItem}
        style={styles.flatList}
      />
    </SafeAreaView>
  );
};

export default memo(Search);
