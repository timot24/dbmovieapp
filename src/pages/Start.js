import React, {useCallback} from 'react';
import {Text, StatusBar} from 'react-native';

import useDidMount from '../hooks/useDidMount';

const Start = ({navigation}) => {
  const requestCategories = useCallback(async () => {
    try {
      StatusBar.setBarStyle('light-content');
      navigation.replace('Home');
    } catch (ex) {
      console.warn(ex);
    }
  }, []);

  useDidMount(() => {
    requestCategories();
  });

  return <Text>Loading...</Text>;
};

export default Start;
