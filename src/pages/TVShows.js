import React, {useCallback, useState, useRef, memo} from 'react';
import {View, FlatList, StyleSheet, ScrollView, StatusBar} from 'react-native';

import {useDispatch, useSelector} from 'react-redux';

import SliderItem from '../components/SliderItem';
import HorizontalMovieCoverList from '../components/HorizontalMovieCoverList';
import useDidMount from '../hooks/useDidMount';

import axios from 'axios';

// import action
import listTVShowsAction from '../redux/actions/TVShows';

const styles = StyleSheet.create({
  title: {
    fontSize: 24,
    fontWeight: 'bold',
    color: 'white',
    marginLeft: 16,
  },
  subtitle: {
    fontSize: 16,
    color: '#ffffff55',
    marginLeft: 16,
    marginBottom: 8,
  },
  horizontalSpacing: {
    width: 8,
  },
  horizontalCoverList: {
    paddingHorizontal: 16,
    paddingBottom: 16,
  },
  flatlist: {
    marginBottom: 16,
  },
});

const KEY_EXTRACTOR = (item) => String(item.id);

const TVShows = () => {
  const [trendingDay, setTrendingDay] = useState(null);

  const flatlistRef = useRef(null);
  const dispatch = useDispatch();

  const {
    trendingTvDay,
    trendingTvWeek,
    discover,
    topRated,
    isLoadingDiscover,
    isLoadingTopRated,
    isLoadingWeek,
    isLoadingDay,
  } = useSelector((state) => state.tvShows);

  const requestTrendingDay = useCallback(async () => {
    try {
      await dispatch(
        listTVShowsAction.tvShows(
          'https://api.themoviedb.org/3/trending/tv/day',
          {
            api_key: '8c6570a65b2a0c3dadfdac7de4cd5d50',
            language: 'en-US',
          },
          'GET_TRENDING_TV_DAY',
        ),
      );
    } catch (ex) {
      console.warn(ex);
    }
  }, []);

  React.useEffect(() => {
    if (!isLoadingDay) {
      const newData = {
        ...trendingTvDay,
        results: trendingTvDay.results
          ? trendingTvDay.results.filter((item) => Boolean(item.backdrop_path))
          : [],
      };
      setTrendingDay(newData);
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [isLoadingDay]);

  const requestTrendingWeek = useCallback(async () => {
    try {
      await dispatch(
        listTVShowsAction.tvShows(
          'https://api.themoviedb.org/3/trending/tv/week',
          {
            api_key: '8c6570a65b2a0c3dadfdac7de4cd5d50',
            language: 'en-US',
          },
          'GET_TRENDING_TV_WEEK',
        ),
      );
    } catch (ex) {
      console.warn(ex);
    }
  }, []);

  const requestDiscover = useCallback(async () => {
    try {
      await dispatch(
        listTVShowsAction.tvShows(
          'https://api.themoviedb.org/3/discover/tv/',
          {
            api_key: '8c6570a65b2a0c3dadfdac7de4cd5d50',
            language: 'en-US',
          },
          'GET_DISCOVER_TV',
        ),
      );
    } catch (ex) {
      console.warn(ex);
    }
  }, []);

  const requestTopRated = useCallback(async () => {
    try {
      await dispatch(
        listTVShowsAction.tvShows(
          'https://api.themoviedb.org/3/tv/top_rated',
          {
            api_key: '8c6570a65b2a0c3dadfdac7de4cd5d50',
            language: 'en-US',
            region: 'ID',
          },
          'GET_TOP_RATED_TV',
        ),
      );
    } catch (ex) {
      console.warn(ex);
    }
  }, []);

  const renderItem = useCallback(
    ({item}) => <SliderItem item={item} mediaType="tv" />,
    [],
  );

  const renderEmptyComponent = () => (
    <View style={styles.placeholderContainer}>
      <SliderItem.Placeholder />
    </View>
  );

  React.useEffect(() => {
    requestTrendingDay();
    requestTrendingWeek();
    requestDiscover();
    requestTopRated();
  }, []);

  return (
    <ScrollView>
      <StatusBar backgroundColor="#00000044" translucent />
      <FlatList
        ref={flatlistRef}
        keyExtractor={KEY_EXTRACTOR}
        data={trendingDay?.results}
        horizontal
        renderItem={renderItem}
        pagingEnabled
        showsHorizontalScrollIndicator={false}
        style={{marginBottom: 16}}
        ListEmptyComponent={renderEmptyComponent}
      />
      <HorizontalMovieCoverList
        title="Best of the week"
        description="See which are the best series of the week"
        showIndex
        data={trendingTvWeek?.results}
        loading={isLoadingWeek}
        mediaType="tv"
      />
      <HorizontalMovieCoverList
        title="Discover"
        description="Descubra ótimas séries para assistir"
        data={discover?.results}
        loading={isLoadingDiscover}
        mediaType="tv"
      />
      <HorizontalMovieCoverList
        title="Top rated"
        description="See which are the most voted series in Brazil"
        data={topRated?.results}
        loading={isLoadingTopRated}
        mediaType="tv"
      />
    </ScrollView>
  );
};

export default memo(TVShows);
