import React, {useCallback, useState, useRef, useEffect, memo} from 'react';
import {View, FlatList, StyleSheet, ScrollView, StatusBar} from 'react-native';
import {useDispatch, useSelector} from 'react-redux';

import SliderItem from '../components/SliderItem';
import HorizontalMovieCoverList from '../components/HorizontalMovieCoverList';

// import action
import listMoviesAction from '../redux/actions/movies';

const styles = StyleSheet.create({
  title: {
    fontSize: 24,
    fontWeight: 'bold',
    color: 'white',
    marginLeft: 16,
  },
  subtitle: {
    fontSize: 16,
    color: '#ffffff55',
    marginLeft: 16,
    marginBottom: 8,
  },
  horizontalSpacing: {
    width: 8,
  },
  horizontalCoverList: {
    paddingHorizontal: 16,
    paddingBottom: 16,
  },
  flatlist: {
    marginBottom: 16,
  },
});

const KEY_EXTRACTOR = (item) => String(item.id);

const Movies = () => {
  const [trendingDay, setTrendingDay] = useState(null);

  const flatlistRef = useRef(null);
  const dispatch = useDispatch();

  useEffect(() => {
    requestNowPlaying();
    requestTrendingDay();
    requestTrendingWeek();
    requestDiscover();
    requestTopRated();
  }, []);

  const {
    isLoadingDay,
    isLoadingWeek,
    isloadingNowPlaying,
    isLoadingDiscover,
    isLoadingTopRated,
    trendingMovieDay,
    trendingMovieWeek,
    nowPlaying,
    topRated,
    discover,
  } = useSelector((state) => state.movies);

  const requestTrendingDay = useCallback(async () => {
    try {
      await dispatch(
        listMoviesAction.movies(
          'https://api.themoviedb.org/3/trending/all/day',
          {
            api_key: '8c6570a65b2a0c3dadfdac7de4cd5d50',
            language: 'en-US',
          },
          'GET_TRENDING_MOVIES_DAY',
        ),
      );
    } catch (ex) {
      console.warn(ex);
    }
  }, []);

  useEffect(() => {
    if (!isLoadingDay) {
      const newData = {
        ...trendingMovieDay,
        results: trendingMovieDay.results
          ? trendingMovieDay.results.filter((item) =>
              Boolean(item.backdrop_path),
            )
          : [],
      };
      setTrendingDay(newData);
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [isLoadingDay]);

  const requestTrendingWeek = useCallback(async () => {
    try {
      await dispatch(
        listMoviesAction.movies(
          'https://api.themoviedb.org/3/trending/movie/week',
          {
            api_key: '8c6570a65b2a0c3dadfdac7de4cd5d50',
            language: 'en-US',
          },
          'GET_TRENDING_MOVIES_WEEK',
        ),
      );
    } catch (ex) {
      console.warn(ex);
    }
  }, []);

  const requestNowPlaying = async () => {
    try {
      await dispatch(
        listMoviesAction.movies(
          'https://api.themoviedb.org/3/movie/now_playing',
          {
            api_key: '8c6570a65b2a0c3dadfdac7de4cd5d50',
            language: 'en-US',
          },
          'GET_NOW_PLAYING',
        ),
      );
    } catch (ex) {
      console.warn(ex);
    }
  };

  const requestDiscover = useCallback(async () => {
    try {
      await dispatch(
        listMoviesAction.movies(
          'https://api.themoviedb.org/3/discover/movie/',
          {
            api_key: '8c6570a65b2a0c3dadfdac7de4cd5d50',
            language: 'en-US',
          },
          'GET_DISCOVER',
        ),
      );
    } catch (ex) {
      console.warn(ex);
    }
  }, []);

  const requestTopRated = useCallback(async () => {
    try {
      await dispatch(
        listMoviesAction.movies(
          'https://api.themoviedb.org/3/movie/top_rated',
          {
            api_key: '8c6570a65b2a0c3dadfdac7de4cd5d50',
            language: 'en-US',
            region: 'ID',
          },
          'GET_TOP_RATED',
        ),
      );
    } catch (ex) {
      console.warn(ex);
    }
  }, []);

  const renderItem = useCallback(
    ({item}) => <SliderItem item={item} mediaType="movie" />,
    [],
  );

  const renderEmptyComponent = () => (
    <View style={styles.placeholderContainer}>
      <SliderItem.Placeholder />
    </View>
  );

  return (
    <ScrollView>
      <StatusBar backgroundColor="#00000044" translucent />
      <FlatList
        ref={flatlistRef}
        keyExtractor={KEY_EXTRACTOR}
        data={trendingDay?.results}
        horizontal
        renderItem={renderItem}
        pagingEnabled
        showsHorizontalScrollIndicator={false}
        style={{marginBottom: 16}}
        ListEmptyComponent={renderEmptyComponent}
      />
      <HorizontalMovieCoverList
        title="Best of the week"
        description="See which are the best films of the week"
        data={trendingMovieWeek?.results}
        loading={isLoadingWeek}
        mediaType="movie"
      />
      <HorizontalMovieCoverList
        title="Now Playing"
        description="See which movie is playing"
        data={nowPlaying?.results}
        loading={isloadingNowPlaying}
        mediaType="movie"
      />
      <HorizontalMovieCoverList
        title="Discover"
        description="Discover great movies to watch"
        data={discover?.results}
        loading={isLoadingDiscover}
        mediaType="movie"
      />
      <HorizontalMovieCoverList
        title="Top rated"
        description="See which are the most voted films in Indonesia"
        data={topRated?.results}
        loading={isLoadingTopRated}
        mediaType="movie"
      />
    </ScrollView>
  );
};

export default memo(Movies);
