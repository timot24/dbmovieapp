import http from '../../helpers/http';

export default {
  tvShows: (query = '', params = {}, type = '') => ({
    type: type,
    payload: http().get(query, {
      params,
    }),
  }),
};
