import http from '../../helpers/http';

export default {
  trailer: (query = '', params = {}, type = '') => ({
    type: type,
    payload: http().get(query, {
      params,
    }),
  }),
};
