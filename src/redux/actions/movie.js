import http from '../../helpers/http';

export default {
  movie: (query = '', params = {}, type = '') => ({
    type: type,
    payload: http().get(query, {
      params,
    }),
  }),
};
