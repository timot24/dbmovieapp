import http from '../../helpers/http';

export default {
  search: (query = '', params = {}, type = '') => ({
    type: type,
    payload: http().get(query, {
      params,
    }),
  }),
};
