import http from '../../helpers/http';

export default {
  tvShow: (query = '', params = {}, type = '') => ({
    type: type,
    payload: http().get(query, {
      params,
    }),
  }),
};
