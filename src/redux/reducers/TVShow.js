const initialState = {
  details: [],
  credits: [],
  related: [],
  isLoadingCredit: false,
  isLoadingDetails: false,
  isLoadingRelated: false,
  isError: false,
  alertMsg: '',
  isSuccess: false,
};

export default (state = initialState, action) => {
  switch (action.type) {
    case 'GET_DETAILS_TV_PENDING': {
      return {
        ...state,
        isLoadingDetails: true,
        isError: false,
        alertMsg: 'Loading ...',
        details: [],
      };
    }
    case 'GET_DETAILS_TV_FULFILLED': {
      return {
        ...state,
        isLoadingDetails: false,
        isError: false,
        alertMsg: 'Success',
        isSuccess: true,
        details: action.payload.data,
      };
    }
    case 'GET_DETAILS_TV_REJECTED': {
      return {
        ...state,
        isLoadingDetails: false,
        isError: true,
        isSuccess: false,
        alertMsg: 'error...',
      };
    }
    case 'GET_CREDIT_TV_PENDING': {
      return {
        ...state,
        isLoadingCredit: true,
        isError: false,
        alertMsg: 'Loading ...',
        credits: [],
      };
    }
    case 'GET_CREDIT_TV_FULFILLED': {
      return {
        ...state,
        isLoadingCredit: false,
        isError: false,
        alertMsg: 'Success',
        isSuccess: true,
        credits: action.payload.data,
      };
    }
    case 'GET_CREDIT_TV_REJECTED': {
      return {
        ...state,
        isLoadingCredit: false,
        isError: true,
        isSuccess: false,
        alertMsg: 'error...',
      };
    }
    case 'GET_RELATED_TV_PENDING': {
      return {
        ...state,
        isLoadingRelated: true,
        isError: false,
        alertMsg: 'Loading ...',
        isSuccess: false,
        related: [],
      };
    }
    case 'GET_RELATED_TV_FULFILLED': {
      return {
        ...state,
        isLoadingRelated: false,
        isError: false,
        alertMsg: 'Success',
        isSuccess: true,
        related: action.payload.data,
      };
    }
    case 'GET_RELATED_TV_REJECTED': {
      return {
        ...state,
        isLoadingRelated: false,
        isError: true,
        isSuccess: false,
        alertMsg: 'error...',
      };
    }
    default: {
      return state;
    }
  }
};
