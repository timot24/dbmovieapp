import {combineReducers} from 'redux';

import movies from './movies';
import movie from './movie';
import tvShows from './TVShows';
import tvShow from './TVShow';
import search from './search';
import trailer from './trailer';

export default combineReducers({
  movies,
  movie,
  tvShows,
  tvShow,
  search,
  trailer,
});
