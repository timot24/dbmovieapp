const initialState = {
  trendingMovieDay: [],
  trendingMovieWeek: [],
  nowPlaying: [],
  discover: [],
  topRated: [],
  isLoadingDiscover: false,
  isLoadingTopRated: false,
  isLoadingWeek: false,
  isLoadingDay: false,
  isLoadingNowPlaying: false,
  isError: false,
  alertMsg: '',
  isSuccess: false,
};

export default (state = initialState, action) => {
  switch (action.type) {
    case 'GET_TRENDING_MOVIES_DAY_PENDING': {
      return {
        ...state,
        isLoadingDay: true,
        isError: false,
        alertMsg: 'Loading ...',
        trendingMovieDay: [],
      };
    }
    case 'GET_TRENDING_MOVIES_DAY_FULFILLED': {
      return {
        ...state,
        isLoadingDay: false,
        isError: false,
        alertMsg: 'Success',
        trendingMovieDay: action.payload.data,
      };
    }
    case 'GET_TRENDING_MOVIES_DAY_REJECTED': {
      return {
        ...state,
        isLoadingDay: false,
        isError: true,
        alertMsg: 'error...',
      };
    }
    case 'GET_TRENDING_MOVIES_WEEK_PENDING': {
      return {
        ...state,
        isLoadingWeek: true,
        isError: false,
        alertMsg: 'Loading ...',
        trendingMovieWeek: [],
      };
    }
    case 'GET_TRENDING_MOVIES_WEEK_FULFILLED': {
      return {
        ...state,
        isLoadingWeek: false,
        isError: false,
        alertMsg: 'Success',
        trendingMovieWeek: action.payload.data,
      };
    }
    case 'GET_TRENDING_MOVIES_WEEK_REJECTED': {
      return {
        ...state,
        isLoadingWeek: false,
        isError: true,
        alertMsg: 'error...',
      };
    }
    case 'GET_NOW_PLAYING_PENDING': {
      return {
        ...state,
        isLoadingNowPlaying: true,
        isError: false,
        alertMsg: 'Loading ...',
        nowPlaying: [],
      };
    }
    case 'GET_NOW_PLAYING_FULFILLED': {
      return {
        ...state,
        isLoadingNowPlaying: false,
        isError: false,
        alertMsg: 'Success',
        nowPlaying: action.payload.data,
      };
    }
    case 'GET_NOW_PLAYING_REJECTED': {
      return {
        ...state,
        isLoadingNowPlaying: false,
        isError: true,
        alertMsg: 'error...',
      };
    }
    case 'GET_DISCOVER_PENDING': {
      return {
        ...state,
        isLoadingDiscover: true,
        isError: false,
        alertMsg: 'Loading ...',
        discover: [],
      };
    }
    case 'GET_DISCOVER_FULFILLED': {
      return {
        ...state,
        isLoadingDiscover: false,
        isError: false,
        alertMsg: 'Success',
        discover: action.payload.data,
      };
    }
    case 'GET_DISCOVER_REJECTED': {
      return {
        ...state,
        isLoadingDiscover: false,
        isError: true,
        alertMsg: 'error...',
      };
    }
    case 'GET_TOP_RATED_PENDING': {
      return {
        ...state,
        isLoadingTopRated: true,
        isError: false,
        alertMsg: 'Loading ...',
        topRated: [],
      };
    }
    case 'GET_TOP_RATED_FULFILLED': {
      return {
        ...state,
        isLoadingTopRated: false,
        isError: false,
        alertMsg: 'Success',
        topRated: action.payload.data,
      };
    }
    case 'GET_TOP_RATED_REJECTED': {
      return {
        ...state,
        isLoadingTopRated: false,
        isError: true,
        alertMsg: 'error...',
      };
    }
    default: {
      return state;
    }
  }
};
