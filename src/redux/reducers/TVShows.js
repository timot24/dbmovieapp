const initialState = {
  trendingTvDay: [],
  trendingTvWeek: [],
  discover: [],
  topRated: [],
  isLoadingDiscover: false,
  isLoadingTopRated: false,
  isLoadingWeek: false,
  isLoadingDay: false,
  isError: false,
  alertMsg: '',
  isSuccess: false,
};

export default (state = initialState, action) => {
  switch (action.type) {
    case 'GET_TRENDING_TV_DAY_PENDING': {
      return {
        ...state,
        isLoadingDay: true,
        isError: false,
        alertMsg: 'Loading ...',
        trendingTvDay: [],
      };
    }
    case 'GET_TRENDING_TV_DAY_FULFILLED': {
      return {
        ...state,
        isLoadingDay: false,
        isError: false,
        alertMsg: 'Success',
        trendingTvDay: action.payload.data,
      };
    }
    case 'GET_TRENDING_TV_DAY_REJECTED': {
      return {
        ...state,
        isLoadingDay: false,
        isError: true,
        alertMsg: 'error...',
      };
    }
    case 'GET_TRENDING_TV_WEEK_PENDING': {
      return {
        ...state,
        isLoadingWeek: true,
        isError: false,
        alertMsg: 'Loading ...',
        trendingTvWeek: [],
      };
    }
    case 'GET_TRENDING_TV_WEEK_FULFILLED': {
      return {
        ...state,
        isLoadingWeek: false,
        isError: false,
        alertMsg: 'Success',
        trendingTvWeek: action.payload.data,
      };
    }
    case 'GET_TRENDING_TV_WEEK_REJECTED': {
      return {
        ...state,
        isLoadingWeek: false,
        isError: true,
        alertMsg: 'error...',
      };
    }
    case 'GET_DISCOVER_TV_PENDING': {
      return {
        ...state,
        isLoadingDiscover: true,
        isError: false,
        alertMsg: 'Loading ...',
        discover: [],
      };
    }
    case 'GET_DISCOVER_TV_FULFILLED': {
      return {
        ...state,
        isLoadingDiscover: false,
        isError: false,
        alertMsg: 'Success',
        discover: action.payload.data,
      };
    }
    case 'GET_DISCOVER_TV_REJECTED': {
      return {
        ...state,
        isLoadingDiscover: false,
        isError: true,
        alertMsg: 'error...',
      };
    }
    case 'GET_TOP_RATED_TV_PENDING': {
      return {
        ...state,
        isLoadingTopRated: true,
        isError: false,
        alertMsg: 'Loading ...',
        topRated: [],
      };
    }
    case 'GET_TOP_RATED_TV_FULFILLED': {
      return {
        ...state,
        isLoadingTopRated: false,
        isError: false,
        alertMsg: 'Success',
        topRated: action.payload.data,
      };
    }
    case 'GET_TOP_RATED_TV_REJECTED': {
      return {
        ...state,
        isLoadingTopRated: false,
        isError: true,
        alertMsg: 'error...',
      };
    }
    default: {
      return state;
    }
  }
};
