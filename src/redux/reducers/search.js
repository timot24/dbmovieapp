const initialState = {
  data: [],
  isLoadingSearch: false,
  isError: false,
  alertMsg: '',
  isSuccess: false,
};

export default (state = initialState, action) => {
  switch (action.type) {
    case 'GET_SEARCH_PENDING': {
      return {
        ...state,
        isLoadingSearch: true,
        isError: false,
        alertMsg: 'Loading ...',
        data: [],
      };
    }
    case 'GET_SEARCH_FULFILLED': {
      return {
        ...state,
        isLoadingSearch: false,
        isError: false,
        alertMsg: 'Success',
        isSuccess: true,
        data: action.payload.data,
      };
    }
    case 'GET_SEARCH_REJECTED': {
      return {
        ...state,
        isLoadingSearch: false,
        isError: true,
        isSuccess: false,
        alertMsg: 'error...',
      };
    }
    default: {
      return state;
    }
  }
};
