const initialState = {
    data: '',
    isLoadingTrailer: false,
    isError: false,
    alertMsg: '',
    isSuccess: false,
  };
  
  export default (state = initialState, action) => {
    switch (action.type) {
      case 'GET_TRAILER_PENDING': {
        return {
          ...state,
          isLoadingTrailer: true,
          isError: false,
          alertMsg: 'Loading ...',
          data: '',
        };
      }
      case 'GET_TRAILER_FULFILLED': {
        return {
          ...state,
          isLoadingTrailer: false,
          isError: false,
          alertMsg: 'Success',
          isSuccess: true,
          data: action.payload.data,
        };
      }
      case 'GET_TRAILER_REJECTED': {
        return {
          ...state,
          isLoadingTrailer: false,
          isError: true,
          isSuccess: false,
          alertMsg: 'error...',
        };
      }
      default: {
        return state;
      }
    }
  };
  