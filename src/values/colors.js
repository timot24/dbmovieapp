const colors = {
  primary: 'rgb(255, 45, 85)',
  background: '#FF7314',
  backgroundLight: '#F4F4F4',
  textPrimary: '#F4F4F4',
  textSecondary: 'rgb(199, 199, 204)',
  disabledText: '#43454f',
};

export default colors;
