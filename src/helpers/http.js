import {default as axios} from 'axios';

function http() {
  // console.log(params);
  return axios.create({
    baseURL: 'https://api.themoviedb.org/3/',
  });
}

export default http;
